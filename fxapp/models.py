from .app import db


INSTITUTIONS = [
    {"name": "bmo", "scrape_type": "web", "scraping_enable": True},
    {"name": "transferwise", "scrape_type": "api", "scraping_enable": True},
    {"name": "remitr", "scrape_type": "api", "scraping_enable": True},
]


class Rate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    institution_name = db.Column(db.String)
    timestamp = db.Column(db.DateTime)
    src_country = db.Column(db.String)  # 3 char code
    dest_country = db.Column(db.String)  # 3 char code
    fee = db.Column(db.Numeric(8, 2))
    rate = db.Column(db.Numeric(8, 2))
