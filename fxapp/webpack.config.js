const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

// JS Config
// ---------
const jsconfig = {
    entry: {
        main: path.join(__dirname, 'static', 'js', 'main.js'),
    },
    output: {
        path: path.join(__dirname, 'static', 'build'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },
    devtool: 'source-map',
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor'
        }),
        new UglifyJSPlugin(),
    ]
};


// CSS Config
// ----------
const cssconfig = {
    entry: {
        styles: path.join(__dirname, 'static' , 'css', 'styles.css'),
    },
    output: {
        path: path.join(__dirname, 'static', 'build'),
        filename: '[name].css'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                                minimize: true,
                            }
                        },
                        'postcss-loader'
                    ]
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('[name].css')
    ]
};


module.exports = [cssconfig, jsconfig];
