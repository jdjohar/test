from decimal import Decimal


def quantize(value):
    return Decimal(str(value)).quantize(Decimal("1.00"))
