import logging

import pendulum
from flask import render_template, redirect, url_for, request

from .app import app, country_data
from .services import rates


logger = logging.getLogger(__name__)

FEATURED_COUNTRIES = ["mex", "usa", "ind", "jpn", "deu", "gbr"]


def render(template_name: str, template_args={}):
    cd = country_data
    featured_countries = []
    for code in FEATURED_COUNTRIES:
        name = cd.get_name_from_country_code(code)
        country_details = {
            "name": name,
            "code": code,
            "url_name": name.replace(" ", "-").lower(),
        }
        featured_countries.append(country_details)

    args = {**template_args, "featured_countries": featured_countries}
    return render_template(template_name, **args)


# Public Routes
# -----------------------------------------------
@app.route("/")
def home():
    cd = country_data
    countries = [
        {
            "name": c["countryName"],
            "code": c["countryCode"],
            "url_name": c["countryName"].replace(" ", "-").lower(),
        }
        for c in cd.country_data
    ]
    return render("home.html", template_args=dict(countries=countries))


@app.route("/ca/send-money-from-<src>-to-<dest>/")
def rates_list(src, dest):
    cd = country_data

    src_country_code = cd.get_code_from_country_name(src).lower()
    dest_country_code = cd.get_code_from_country_name(dest).lower()

    amount = request.args.get("amount", 100)
    _rates = rates.get_rates(src_country_code, dest_country_code, amount)

    return render(
        "rates.html",
        template_args=dict(
            rates=_rates,
            src_country=src.title().replace("-", " "),
            dest_country=dest.title().replace("-", " "),
            src_country_code=src_country_code,
            dest_country_code=dest_country_code,
            amount=amount,
            last_updated=pendulum.now("America/Toronto").to_day_datetime_string(),
        ),
    )


@app.route("/send-money", methods=["POST"])
def rates_post():
    amount = request.form["amount"]
    dest = request.form["destination_country_code"]
    return redirect(url_for("rates_list", src="canada", dest=dest, amount=amount))


# Private Routes
# -----------------------------------------------
