import logging
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from .services.countrydata import CountryData


FX_LOG_LEVEL = os.environ.get("FX_LOG_LEVEL", logging.WARNING)

logging.basicConfig(level=FX_LOG_LEVEL)

app = Flask(__name__)

# Setup Database
# ---
app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{app.root_path}/fxratehunter.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
# ---

country_data = CountryData()

from . import routes  # noqa

db.create_all()
