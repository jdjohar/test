import json
import os


BASE_PATH = os.path.dirname(os.path.dirname(__file__))


class CountryData:
    def __init__(self, country_data=None):
        if not country_data:
            file_path = os.path.join(BASE_PATH, "data", "countries.json")
            with open(file_path, "r") as file:
                country_data = json.load(file)
        self.country_data = country_data

    def get_name_from_currency_code(self, currency_code=""):
        for data in self.country_data:
            if data["currencyCode"].lower() == currency_code.lower():
                return data["countryName"]

    def get_name_from_country_code(self, country_code=""):
        for data in self.country_data:
            if data["isoAlpha3"].lower() == country_code.lower():
                return data["countryName"]

    def get_currency_from_country_code(self, country_code=""):
        for data in self.country_data:
            if data["isoAlpha3"].lower() == country_code.lower():
                return data["currencyCode"]

    def get_currency_from_country_name(self, country_name=""):
        country_name = country_name.replace("-", " ")

        for data in self.country_data:
            if data["countryName"].lower() == country_name.lower():
                return data["currencyCode"]

    def get_code_from_country_name(self, country_name=""):
        country_name = country_name.replace("-", " ")

        for data in self.country_data:
            if data["countryName"].lower() == country_name.lower():
                return data["isoAlpha3"]
