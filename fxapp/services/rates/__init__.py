import logging
import pprint
from decimal import Decimal

from fxapp import models
from fxapp.app import country_data
from .api_fetcher import RateFetcher


logger = logging.getLogger(__name__)


def get_rates(src_country_code, dest_country_code, amount=100):
    """
    Returns all rates: API and scraped
    """
    # Fetch API rates
    api_rates = get_api_rates(src_country_code, dest_country_code, amount)
    return api_rates


def get_api_rates(src_country_code, dest_country_code, amount):
    """
    Example return value:
    [
        {
            'dest_country_code': 'ind',
            'src_country_code': 'can'
            'fee': Decimal('8.59'),
            'rate': Decimal('56.11'),
            'amount_receivable': '',
            'institution': {
                'name': 'transferwise',
                'scrape_type': 'api',
                'scraping_enable': True
            },

        },
    ]
    """
    cd = country_data

    src_currency = cd.get_currency_from_country_code(src_country_code)
    dest_currency = cd.get_currency_from_country_code(dest_country_code)

    api_institutions = [
        inst for inst in models.INSTITUTIONS if inst["scrape_type"] == "api"
    ]

    rates = RateFetcher.fetch_rates(
        api_institutions, src_cur=src_currency, dest_cur=dest_currency, src_amt=amount
    )
    logger.info(f"Rates --->\n{pprint.pformat(rates)}")

    # Add additional details to rate...
    for rate in rates:
        rate["src_country_code"] = src_country_code
        rate["dest_country_code"] = dest_country_code
        rate["src_currency_code"] = src_currency
        rate["dest_currency_code"] = dest_currency
        rate["amount_receivable"] = _calculate_amount_receivable(
            rate["rate"], rate["fee"], amount
        )

    return rates


def get_scraped_rates():
    return []


def _calculate_amount_receivable(rate, transfer_fee, amount):
    print(rate, amount, "-----------")
    return (Decimal(str(rate)) * Decimal(str(amount))) - Decimal(str(transfer_fee))
