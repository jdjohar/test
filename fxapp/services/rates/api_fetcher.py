import logging
import threading
from string import Template

import requests

from .api_parser import RateParser


log = logging.getLogger(__name__)


class RateFetcher:
    API_URLS = {
        "remitr": Template(
            "https://api.remitr.com/api/fxrate/$src_cur-$dest_cur?tk=a4d9t1&format=json&src_amount=$src_amt"
        ),
        "transferwise": Template(
            "https://api.transferwise.com/v1/quotes?source=$src_cur&target=$dest_cur&sourceAmount=$src_amt&rateType=FIXED"
        ),
    }

    @staticmethod
    def worker(institution, args):
        url_template = RateFetcher.API_URLS[institution["name"]]
        url = url_template.substitute(
            src_cur=args["src_cur"], dest_cur=args["dest_cur"], src_amt=args["src_amt"]
        )

        resp = requests.get(url)
        parser = getattr(RateParser, institution["name"])
        result = parser(resp.text)  # Call parser to parse JSON and return result

        if result:
            args["rates"].append(
                {
                    "institution": institution,
                    "fee": result["fee"],
                    "rate": result["rate"],
                }
            )

    @staticmethod
    def fetch_rates(institutions, src_cur, dest_cur, src_amt):
        """Fetches rates in a separate thread for each of the institution names
        provided.

        Exmaple response:
        [
            {
                'institution': {
                    'name': 'transferwise', 
                    'scrape_type': 'api', 
                    'scraping_enable': True
                },
                'fee': Decimal('11.37'),
                'rate': Decimal('55.93')
            }
        ]
        """
        rates = []
        threads = []
        for institution in institutions:
            worker_args = dict(
                src_cur=src_cur, dest_cur=dest_cur, src_amt=src_amt, rates=rates
            )
            t = threading.Thread(
                target=RateFetcher.worker, args=[institution, worker_args]
            )
            t.start()
            threads.append(t)

        # Join worker threads to our main thread so we wait until they are done.
        for t in threads:
            t.join()

        return rates  # threads have finished, rates should now be populated...
