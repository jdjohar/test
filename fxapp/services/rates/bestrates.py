from typing import List, Tuple, Optional
import logging

import redis
import simplejson as json

from core.services.rates import get_rates, RateResult
from core.apps import get_redis_client


log = logging.getLogger("bestrates")


KEY_PREFIX = "bestrate:"


def get_best_rate_for_country_code(src: str, dest: str) -> Optional[dict]:
    """Gets the best rate from storage for the src/dest country codes provided.

    Args:
        src (str): Source country code
        dest (str): Destination country code

    Returns:
        dict - For keys, see core.services.rates.RateResult.serialize()
    """
    redis_client = get_redis_client()
    key = _generate_key(src, dest)
    rate = redis_client.get(key)
    if rate:
        return json.loads(rate)


def get_best_rates_for_country_codes(
    country_codes: List[Tuple[str, str]]
) -> List[dict]:
    """Given a list of src/dest currencies, returns the best rates

    Args:
        country_codes: List of Tuples - (src, dest)

    Returns:
        List of Dicts - For keys, see core.services.rates.RateResult.serialize()
    """
    rates = []
    for src, dest in country_codes:
        rate = get_best_rate_for_country_code(src, dest)
        if rate:
            rates.append(rate)
    return rates


def get_best_rates() -> List[dict]:
    """Gets all the best rates in storage

    Returns
        [dict(), ...] - For keys, see core.services.rates.RateResult.serialize()
        Note: None will be placed in list if key not present
    """
    redis_client = get_redis_client()
    key_wildcard = KEY_PREFIX + "*"
    keys = redis_client.keys(key_wildcard)

    if not keys:
        # We can't operate on an empty list of keys so return early if that's the case
        return []

    rates: List[Optional[str]] = redis_client.mget(keys)

    # Return a list of rates + ensure no rate is empty (ie. None)
    return [json.loads(rate) for rate in rates if rate]


def _generate_key(src_cur: str, dest_cur: str) -> str:
    """Generates the key used for storing bestrate entries in Redis.

    Given a src_cur and dest_cur, produces the following key:
    bestrate:cad-inr

    Args:
        src_cur (str) - Three letter source currency
        dest_cur (str) - Three letter destination currency
    """
    return KEY_PREFIX + src_cur.lower() + "-" + dest_cur.lower()


def _store_rate_in_redis(
    serialized_rate: str, src: str, dest: str, redis_client: redis.Redis
):
    """Stores the given rate_result in redis.

    Under the following key structure: "bestrate:can-ind"

    Args:
        serialized_rate (str) - JSON string of RateResult
        src (str) - Source country code
        dest (str) - Destination country code
        redis_client (redis.Redis(...)) - Redis client connection object
    """
    key = _generate_key(src, dest)
    redis_client.set(key, serialized_rate)


def populate_best_rates_for_country_codes(
    country_codes: List[Tuple[str, str]]
) -> List[dict]:
    """Fetches the best rate for the currency codes specified and stores them
    in Redis.
    """
    redis_client = get_redis_client()
    best_rates: List[dict] = []
    amount = 100
    for src, dest in country_codes:
        country_codes = dict(src=src, dest=dest)
        rates: List[RateResult] = get_rates(country_codes, amount)
        if rates:
            best_rate = sorted(rates, key=lambda rate: rate.rate, reverse=True)[0]

            serialized_rate = best_rate.serialize()
            best_rates.append(serialized_rate)

            _store_rate_in_redis(serialized_rate, src, dest, redis_client)
    log.info(best_rates)
    return best_rates
