import logging
import simplejson as json

from fxapp import utils


log = logging.getLogger(__name__)


class RateParser:
    @staticmethod
    def _parse(json_resp):
        try:
            return json.loads(json_resp)
        except json.JSONDecodeError as e:
            log.error("JSONDecodeError parsing Remitr resp.")

    @staticmethod
    def remitr(json_resp):
        """Responsible for parsing rate info from the Remitr API response"""
        # Sample response
        # {"status":200,"version":1,"result":{"transfer_rate":50.98706,"transfer_fee":0}}
        resp = RateParser._parse(json_resp)
        if resp and "result" in resp:
            rate = utils.quantize(resp["result"].get("transfer_rate", 0))
            fee = utils.quantize(resp["result"].get("transfer_fee", 0))
            return {"rate": rate, "fee": fee}
        else:
            log.warning("No Remitr response to parse.")

    @staticmethod
    def transferwise(json_resp):
        """Responsible for parsing rate info from the TransferWise API resp."""
        # Sample response
        # {"source":"CAD","target":"INR","sourceAmount":100.0,"targetAmount":4970.65,"type":"REGULAR",
        # "rate":51.24385,"createdTime":"2018-02-24T21:55:11.533Z","rateType":"FIXED",
        # "deliveryEstimate":"2018-02-28T05:15:00Z","fee":3.0,"allowedProfileTypes":["PERSONAL","BUSINESS"],
        # "guaranteedTargetAmount":false,"ofSourceAmount":true}
        resp = RateParser._parse(json_resp)
        if resp and "source" in resp:
            return {
                "rate": utils.quantize(resp.get("rate", 0)),
                "fee": utils.quantize(resp.get("fee", 0)),
            }
        else:
            log.warning("No TransferWise response to parse")
