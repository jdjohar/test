module.exports = {
    plugins: [
        require('precss'),
        require('autoprefixer'),
        require('lost'),
        require('postcss-color-function'),
    ]
}
