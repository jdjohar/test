const iconNames = ['far fa-money-bill-alt', 'fas fa-money-bill-alt'];

const rotations = [
    20, 30, 60, 80, 110, 130, 150, 170, 200, 220, 240, 260, 280, 300, 320, 340,
    -20, -30, -60, -80, -110, -130, -150, -170, -200, -220, -240, -260, -280, -300, -320, -340,
];

const sizes = ['', 'fa-sm', 'fa-lg', 'fa-2x', 'fa-3x'];

const colors = ['F5F5F5', 'EEE'];  // LightBlue -> E6F3FE  LightGreen -> F0F6F6  LightYellow -> FFF8CF

export class HeroIcons {
    constructor($container, gridSize=50) {
        this.xPositions = [];
        this.yPositions = [];
        this.$container = $container;
        this.gridSize = 50;
        this.grid = this.setupGrid($container);
    }

    /**
     * Insert randomly generated icons into the container.
     */
    insert(numToInsert) {
        const icons = [];

        for (let i = 0; i < numToInsert; i++) {

            // Step 1 - Get icon type
            const iconName = iconNames[_getRandomInt(0, iconNames.length)];

            // Step 2 - Get icon rotation
            const rotation = rotations[_getRandomInt(0, rotations.length)];

            // Step 3 - Get icon size
            const iconSize = sizes[_getRandomInt(0, sizes.length)];

            // - Get icon color
            const iconColor = colors[_getRandomInt(0, colors.length)];

            // Step 4 - Get icon position
            const { gridX, gridY } = this.getAvailableGridPosition();
            const pos = this.translateGridPosToCssPos(gridX, gridY);

            // Step 5 - Build icon element (with infor above)
            const icon = $('<i>', {
                'class': `${iconName} ${iconSize}`,
                'style': `position:absolute;top:${pos.top}px;left:${pos.left}px;color:#${iconColor};`,
                'data-fa-transform': `rotate-${rotation}`,
            });

            icons.push(icon);
        }

        // Step 5 - Append all created icon elements into container
        this.$container.append(icons);
    }

    /**
     * Based on the dimensions of the container, setup a grid of this.gridSize
     * cells and return this grid as an array matrix...
     *
     * For example, a 3 x 3 grid:
     * grid = [
     *     [0, 0, 0],
     *     [0, 0 , 0],
     *     [0, 0, 0],
     * ]
     */
    setupGrid() {
        const grid = [];
        const { width, height } = this._getBoundingBox();
        const xCols = Math.floor(width / this.gridSize);
        const yCols = Math.floor(height / this.gridSize);

        for (let i = 0; i < yCols; i++) {
            const xColsArray = this._createGridArray(xCols);
            grid.push(xColsArray);
        }
        return grid;
    }

    /**
     * Returns a random grid position/slot that is free (no item present)
     */
    getAvailableGridPosition() {
        const numGridRows = this.grid.length;
        const numGridCols = this.grid[0].length;

        let gridX = _getRandomInt(0, numGridCols);
        let gridY = _getRandomInt(0, numGridRows);


        while (!this._isGridPosAvailable(gridX, gridY)) {
            gridX = _getRandomInt(0, numGridCols);
            gridY = _getRandomInt(0, numGridRows);
        }

        this._markGridPosUnavailable(gridX, gridY);

        return { gridX, gridY };
    }

    /**
     * Returns the appropriate top, and left values (pixels) based on
     * the x and y cell positions provided.
     *
     * Basic algo. --> x (5) * this.gridSize (50px) = 250px (css left);
     */
    translateGridPosToCssPos(x, y) {
        return {
            'top': y * this.gridSize,
            'left': x * this.gridSize,
        }
    }

    _markGridPosUnavailable(x, y) {
        this.grid[y][x] = 1;
    }

    _isGridPosAvailable(x, y) {
        return 0 === this.grid[y][x];
    }

    /**
     * Return a filled array based on
     */
    _createGridArray(length, fillVal=0) {
        const array = []
        for (let i = 0; i < length; i++) {
            array.push(0);
        }
        return array;
    }

    /**
     * Calculate the dimensions of the container element.
     */
    _getBoundingBox() {
        return {
            'width': this.$container.innerWidth(),
            'height': this.$container.innerHeight(),
        }
    }
}

function _getRandomInt(min, max) {
    // Note: max is NOT inclusive
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}
