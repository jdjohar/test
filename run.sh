#!/bin/bash

# Commands
# ---
flask-dev () {
    FLASK_ENV=development FLASK_APP=fxapp.app FX_LOG_LEVEL="DEBUG" pipenv run flask run
}

compile-static () {
    cd fxapp && npx webpack
}

build-web-container () {
    docker build -f web.dockerfile -t fxapp-web .
}

# ---

# store the first arg in the command variable
# and then pop it from the args list.
command=$1
shift

# run our passed-in command and pass it any additional args.
$command "$@"
