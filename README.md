# Project Setup

### Required items for developing
- [Pipenv](https://github.com/pypa/pipenv)
- Docker + Docker Compose
- Python 3


### Installing dependencies
1. `git clone <repo_url>`

2. `pipenv install`

3. Install JS dependencies for fxapp:  
`cd fxapp && npm install`  
Note: `npm` is notorious for stalling.  If it does stall, simply kill it and run `npm install` again. Remember to be in the correct directory.

4. Install JS dependencies for fxscraper:  
`cd fxscraper && npm install` 

5. Pull the latest docker images:  
`docker-compose -f dev-compose.yml pull`


### Running locally
1. Run flask dev server (python): `./run.sh flask-dev`

    That will bring up the flask dev server in "watch" mode so any changes made to python source file will be picked up and the server reloaded.

2. Run webpack to watch for changes made to JS files

    In another terminal window, `cd` into the `fxapp` folder and run: `npx webpack -w`
