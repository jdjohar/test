FROM python:3.6-alpine

# See: https://docs.docker.com/compose/django/
# And also: https://docs.python.org/3/using/cmdline.html#envvar-PYTHONUNBUFFERED
ENV PYTHONUNBUFFERED 1

# Install required deps. for psycopg2
RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev

RUN mkdir -p /var/app/fxratehunter

# Copy over source code into container
COPY Pipfile Pipfile.lock /var/app/fxratehunter/
COPY fxapp /var/app/fxratehunter/fxapp
WORKDIR /var/app/fxratehunter

# Install pip and npm packages...
RUN pip install --upgrade pip \
  && pip install pipenv \
  && pipenv install

# Clean up/remove deps. no longer required to reduce image size.
RUN apk del build-deps

EXPOSE 8000
CMD ["pipenv", "run", "gunicorn", "-b", "0.0.0.0:8000", "-w", "2", "fxapp.app:app"]
