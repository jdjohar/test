const puppeteer = require('puppeteer');


exports.getBrowser = async function () {
    const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });
    console.log("Browser up...");
    return browser;
};

exports.getPage = async function (browser, url) {
    console.log("Going to website...");
    const page = await browser.newPage();
    await page.goto(url);
    return page;
};