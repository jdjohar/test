const axios = require('axios');

// Load all scrapers
const bmo = require('./bmo_scraper');


let webHost = process.env['WEB_HOST'];
if (webHost === undefined) {
    webHost = 'web';
}


const SCRAPER_INFO_URL = `http://${webHost}/private/scraper-info/`;
const UPDATE_RATE_URL = `http://${webHost}/private/update-rates/`;


// Send request to app for list of scrapers to scrape.
axios.get(SCRAPER_INFO_URL)
    .then(function (response) {
        let scraperData = response.data;
        for (let s of scraperData) {
            let scraperName = s.name;
            try {
                // Get a ref. to our scraper (loaded above)
                let scraper = eval(scraperName);
                doScraping(scraper);
            } catch(error) {
                console.log(error);
            }
        }
    })
    .catch(function (error) {
        console.log(error);
    });


function calculateTotalTime(startTime, endTime) {
    return (endTime - startTime) / 1000;
}


async function doScraping(scraper) {
    const startTime = Date.now();
    const scrapedRates = await scraper.Scraper.scrape(scraper.scraperConfig);
    const endTIme = Date.now();
    console.log('---');
    console.log(`Total time to scrape: ${calculateTotalTime(startTime, endTIme)}`);

    // Creates a JSON payload in a structure expected by the server
    const ratePayload = scraper.Scraper.prepareData(scrapedRates);

    // Send the rate payload to the server
    updateRates(ratePayload);
}


function updateRates(ratePayload) {
    // Send request to server
    axios.post(UPDATE_RATE_URL, ratePayload)
        .then(function (response) {
            console.log(response.status);
            console.log(response.data);
        })
        .catch(function (error) {
            console.log(error.response.status);
        });
}
