const common = require('./common');

const scraperConfig = {
    url: 'https://www.bmo.com/home/personal/banking/rates/foreign-exchange'
};

class Scraper {
    static get name() {
        return 'bmo'
    }

    static async scrape(config) {
        const browser = await common.getBrowser();
        const page = await common.getPage(browser, config.url);

        const rates = await page.evaluate(() => {
            const rates = [];
            const rows = document.querySelectorAll('#ratesTable tr.row');
            rows.forEach((e) => {
                let country = e.querySelector('.country-col').textContent;
                let rate = e.querySelector('.sell-col').textContent;
                rates.push({country, rate});
            });
            return rates;
        });

        await browser.close();
        console.log("Browser closed!");

        return rates;
    }

    // Constructs a payload in a structure expected by the server...
    static prepareData(scrapedRates) {
        const ratesData = [];
        for (let rate of scrapedRates) {
            ratesData.push({
                'baseCurrency': 'cad',
                'country': rate.country,
                'rate': rate.rate,
            });
        }
        return {
            'institution': 'bmo',
            'ratesData': ratesData
        }
    }
}

exports.Scraper = Scraper;
exports.scraperConfig = scraperConfig;

