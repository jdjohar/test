# Dockerfile to install Chrome + Node and Crontab -- to be used as a base image
# for headless browsing support.

FROM debian:sid

ENV TZ=America/New_York
#RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
	build-essential \
	apt-transport-https \
	ca-certificates \
	curl \
    gnupg \
    tzdata \
    libgconf-2-4 \
	--no-install-recommends \
	# Install Chrome
	# --------------
	&& curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
	&& echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
	&& apt-get update && apt-get install -y \
	google-chrome-stable \
	--no-install-recommends \
	# Install NodeJS
	# --------------
	&& curl -sL https://deb.nodesource.com/setup_8.x | bash - \
	&& apt-get install -y nodejs --no-install-recommends
	# Clean up -- remove/purge unnecessary items
	# --------------
#	&& apt-get purge --auto-remove -y curl gnupg \
#	&& rm -rf /var/lib/apt/lists/*


# Install Tini
# ------------
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini


# Create dir. to hold application code and install NPM deps.
RUN mkdir -p /var/app
WORKDIR /var/app
COPY ./fxscraper/ .
RUN npm install -dd

# ---

# Install Supersonic Cron
# -----------------------
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.5/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=9aeb41e00cc7b71d30d33c57a2333f2c2581a201

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic
# ---


ADD crontab .
#RUN cat ./crontab | crontab -

ENTRYPOINT ["/tini", "--"]

#CMD cron -f
CMD /usr/local/bin/supercronic --debug crontab